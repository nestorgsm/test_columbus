/**
 * @param {number} num
 * @return {string}
 */
var intToRoman = function(num) {
    const arrNum = [1, 5, 10, 50, 100, 500, 1000];
    const arrLetra = ['I', 'V', 'X', 'L', 'C', 'D', 'M']
    const name = [];
    
    for (var i = 0; i < arrNum.length; i++) {
        if(num >= arrNum[i]) //100
        {
            let res = Math.round(num / arrNum[i]);
            
            num = num - (res * arrNum[i])
            
            //con 2,3 y 4 falla por eso hago este if
            if(res < 4){
                for(var j = 0; j < res; j++){
                    name.push(arrLetra[i])
                }
            } else {
                name.push(arrLetra[i])
            }
            
        }
    }
    return name.join('')
};
